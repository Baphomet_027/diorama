﻿using UnityEngine;

public class SimpleController : MonoBehaviour {

    private CharacterController _character;

    public GameObject goSettingDay;
    public GameObject goSettingNight;

    public float lookSencivility = 10;
    public float speedMove = 10;

    private bool isDay;

    private void Awake() {
        _character = GetComponent<CharacterController>();

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update() {
        PerformMovement();
        PerformRotation();
        ToggleSetting();
        RestPosition();
    }

    private void PerformMovement() {
        float axis_X = Input.GetAxisRaw("Horizontal");
        float axis_Y = Input.GetAxisRaw("Transversal");
        float axis_Z = Input.GetAxisRaw("Vertical");

        Vector3 vRight = transform.right * axis_X;
        Vector3 vUp = transform.up * axis_Y;
        Vector3 vForward = transform.forward * axis_Z;

        Vector3 direction = (vRight + vUp + vForward).normalized;
        Vector3 velocity = direction * speedMove * Time.deltaTime;

        _character.Move(velocity);
    }

    private void PerformRotation() {
        float rot_X = Input.GetAxis("Mouse Y");
        float rot_Y = Input.GetAxis("Mouse X");

        Vector3 rotation = transform.eulerAngles;
        rotation.x -= rot_X * Time.deltaTime * lookSencivility;
        rotation.y += rot_Y * Time.deltaTime * lookSencivility;

        if (rotation.x <= -360 || rotation.x >= 360)
            rotation.x = 0;

        if (rotation.y <= -360 || rotation.y >= 360)
            rotation.y = 0;

        transform.rotation = Quaternion.Euler(rotation);
    }

    private void ToggleSetting() {
        if (Input.GetKeyDown(KeyCode.Tab)) {
            isDay = !isDay;

            goSettingDay.SetActive(isDay);
            goSettingNight.SetActive(!isDay);
        }
    }

    private void RestPosition() {
        if (Input.GetKeyDown(KeyCode.R)) {
            transform.position = Vector3.up;
            transform.rotation = Quaternion.Euler(Vector3.zero);
        }
    }
}